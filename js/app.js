	var myApp = angular.module("crud", ["ngRoute"]);
	myApp.config(function($routeProvider){
		$routeProvider
		.when('/', {
			templateUrl : 'Templates/posts.html',
			controller: 'postsCtrl'
		})
		.when('/createPost', {
			templateUrl : 'Templates/create.html',
			controller: 'createCtrl'
		})
		.when('/post/:id', {
			templateUrl : 'Templates/view.html',
			controller: 'viewCtrl'
		})
		.when('/delete/:id', {
			templateUrl : 'Templates/delete.html',
			controller: 'deleteCtrl'
		})
});
	myApp.controller("postsCtrl", function($scope, $http){
		$http.get("http://127.0.0.1/angularjscrud/webservices/allPosts.php")
		.then(function(response){
			$scope.posts = response.data;
/* 			console.log($scope.posts);  */
	});
});

	myApp.controller("viewCtrl", function($scope, $http, $routeParams){
		$http({
			url: "http://127.0.0.1/angularjscrud/webservices/getPost.php",
			params:{id:$routeParams.id},
			method: "get"
		})
		.then(function(response){
			$scope.posts = response.data;
			console.log($scope.posts);
		});
	});
	
	myApp.controller("deleteCtrl", function($scope, $http, $routeParams){
		$http({
			url: "http://127.0.0.1/angularjscrud/webservices/delete.php",
			params:{id:$routeParams.id},
			method: "get"
		})
		.then(function(response){
			$scope.posts = response.data;
			console.log($scope.posts);
		});
	});
	
	myApp.controller("createCtrl", function($scope){
		$("#submit").click(function(){
			var title = $("#title").val();
			var description = $("#description").val();
			var dataString = $("#myform").serialize();
			if(title == "" || description == ""){
				$("#msg").html("Please fill all details");
			}
			else{
				$.ajax({
				type:'POST',
				url: 'http://127.0.0.1/angularjscrud/webservices/addPost.php',
				data:dataString,
				cache: false,
				success: function(result){
					$("#msg").html(result);
					var title = $("#title").val("");
					var description = $("#description").val("");
				}
				});
			}
			return false;
		});
	});



	/* https://stackoverflow.com/questions/41169385/http-get-success-is-not-a-function */





